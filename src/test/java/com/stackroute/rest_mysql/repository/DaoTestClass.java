package com.stackroute.rest_mysql.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.stackroute.rest_mysql.entities.Customer;

@ExtendWith(MockitoExtension.class)
@DataJpaTest // creating Virtual DB Environment to perform testing on these methods
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DaoTestClass {
	@Autowired
	CustomerRepository repo;
	
	@Test
	public void testInsertSelectDelete() {
		/*
		 * System.out.println(repo); Customer c1=new Customer(1,"Nuthan",15,"Mysore");
		 * c1=repo.save(c1); System.out.println(c1);
		 */
		
		/*
		 * List<Customer> list=repo.findAll(); assertEquals(5,list.size());
		 */
		
		/*
		 * repo.deleteAll(); Assertions.assertThat(repo.findAll().isEmpty());
		 * 
		 * List<Customer> list=repo.findAll(); assertEquals(3,list.size());
		 */
		
		/*
		 * repo.deleteById(1l);
		 * 
		 * assertEquals(repo.getById(1l).getName(),"Varidhi preeth");
		 */
		
	}
}
