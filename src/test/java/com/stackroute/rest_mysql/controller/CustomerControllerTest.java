package com.stackroute.rest_mysql.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.stackroute.rest_mysql.entities.Customer;
import com.stackroute.rest_mysql.service.CustomerServiceImpl;

@ExtendWith(MockitoExtension.class)
@WebMvcTest
class CustomerControllerTest {	
	
	@MockBean
	CustomerServiceImpl service;
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void testSelectAll() throws Exception  {
		List<Customer> list=new ArrayList<Customer>();
		Customer c1=new Customer(1,"King",25,"Hyd");
		Customer c2=new Customer(2,"Queen",20,"TN");
		Customer c3=new Customer(3,"Mohan",25,"KA");
		
		list.add(c1);list.add(c2);list.add(c3);
		
		when(service.selectAllEmployee()).thenReturn(list);
		
		mockMvc.perform(get("/api/v1/customer"))
		               .andExpect(status().isOk())
		               .andExpect(jsonPath("$", Matchers.hasSize(3)));
		               //.andExpect(jsonPath("$[0].name",Matchers.is("King")));
	}
}
