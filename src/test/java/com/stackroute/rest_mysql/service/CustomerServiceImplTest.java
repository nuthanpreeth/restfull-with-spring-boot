package com.stackroute.rest_mysql.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.stackroute.rest_mysql.entities.Customer;
import com.stackroute.rest_mysql.repository.CustomerRepository;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {

	@Mock
	CustomerRepository repo;
	
	@InjectMocks
	CustomerServiceImpl service;
	
	@Test
	void testInsertCustomer() {
		System.out.println("Repository Object="+repo);
		System.out.println("Service Object="+service);
		Customer c1=new Customer(1,"dharshan",15,"Mysore");
		service.insertCustomer(c1);
		verify(repo,times(1)).save(c1);
	}
	
	@Test
	public void testSelectAll() {
		List<Customer> list=new ArrayList<Customer>();
		Customer c1=new Customer(1,"King",25,"Hyd");
		Customer c2=new Customer(2,"Queen",20,"TN");
		Customer c3=new Customer(3,"Mohan",25,"KA");
		
		list.add(c1);list.add(c2);list.add(c3);
		
		when(repo.findAll()).thenReturn(list);
		
		List<Customer> result=service.selectAllEmployee();
		
		verify(repo).findAll();
		
		assertEquals(3,list.size());
		
	}
	
	
}
