package com.stackroute.rest_mysql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import com.stackroute.rest_mysql.controller.CustomerController;
import com.stackroute.rest_mysql.entities.Customer;
import com.stackroute.rest_mysql.repository.CustomerRepository;
import com.stackroute.rest_mysql.service.CustomerServiceImpl;

@SpringBootTest
class RestMysqlApplicationTests {

	//Testing for Repository
	@Autowired
	CustomerRepository repo;
	@Test
    @Disabled
	public void testGetAllCustomers() {
		List<Customer> list=repo.findAll();
		System.out.println(list);
		assertNotNull(list);
	}
	
	
	
	//Testing for Service 
	@Autowired
	CustomerServiceImpl service;
	@Test
	@Disabled
	public void testDeleteCustomer() {
		service.deletById(3);
		assertTrue(true);
	}
	
	//Testing for controller
	@Autowired
	CustomerController controller;
	@Autowired
	RestTemplate rest;
	@Test
	@Disabled
	public void testUpdateCustomer() {
		Customer customer=new Customer();
		customer.setId(2);
		customer.setName("Nuthanpreeth");
		customer.setAge(18);
		customer.setLocation("Mysore");
		//Customer x= controller.updateCustomer(customer);
		//In controller we need to focus on RestApI
		rest.put("http://localhost:8080/api/v1/customer", customer);
		assertTrue(true);
	}
	
	
	@Test
	//@Disabled
	public void testInsertCustomer() {
		Customer customer=new Customer();
		customer.setId(3);
		customer.setAge(35);
		customer.setLocation("Hyd");
		customer.setName("Reddy");
		Customer custObject=controller.insertCustomer(customer);
		assertNotNull(custObject);
		}
}
