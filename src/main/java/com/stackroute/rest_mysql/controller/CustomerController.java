package com.stackroute.rest_mysql.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.stackroute.rest_mysql.entities.Customer;
import com.stackroute.rest_mysql.service.CustomerServiceImpl;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
	@Autowired
	CustomerServiceImpl service;
	
	@PostMapping(value="/customer",consumes = {"application/json"})
    public Customer insertCustomer(@RequestBody Customer customer) {
		return service.insertCustomer(customer);
	}
	@GetMapping(value="/customer",produces = {"application/json"})
	public List<Customer> getAllCustomer(){
		return service.selectAllEmployee();
	}
	@DeleteMapping("/customer/delete/{id}")
	public ResponseEntity<String> deleteById(@PathVariable long id){
		service.deletById(id);
		return new ResponseEntity<String>("Record Deleted",HttpStatus.OK);
	}
	@GetMapping("/customer/{id}")
	public Customer getById(@PathVariable long id) {
		return service.getById(id);
	}
	@PutMapping("/customer")
	public Customer updateCustomer(@RequestBody Customer customer ) {
		return service.updateCustomer(customer);
	}
	
	//In spring 3, we can use RestTemplate to test Http based restfull web services, it doesn't support http protocol
	@Autowired
	RestTemplate restTemplate;
	@GetMapping("/get")
	public ResponseEntity<List> getRecords(){
	 ResponseEntity<List> response=restTemplate.getForEntity("http://localhost:8080/api/v1/customer",List.class);
	 return response;
	}
}
