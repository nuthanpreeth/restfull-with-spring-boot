package com.stackroute.rest_mysql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import com.stackroute.rest_mysql.entities.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {

}
