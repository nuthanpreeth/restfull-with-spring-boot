package com.stackroute.rest_mysql.service;

import java.util.List;

import com.stackroute.rest_mysql.entities.Customer;

public interface ICustomerService {
	
	public Customer insertCustomer(Customer customer);
	public List<Customer> selectAllEmployee();
	public void deletById(long id);
	public Customer getById(long id);
	public Customer updateCustomer(Customer customer);
}
