package com.stackroute.rest_mysql.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.rest_mysql.entities.Customer;
import com.stackroute.rest_mysql.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements ICustomerService{

	@Autowired
	CustomerRepository customerRepository;
	@Override
	public Customer insertCustomer(Customer customer) {
		return customerRepository.save(customer);
	}
	@Override
	public List<Customer> selectAllEmployee() {
		
		return customerRepository.findAll();
	}
	@Override
	public void deletById(long id) {
		customerRepository.deleteById(id);
	}
	@Override
	public Customer getById(long id) {
		return customerRepository.getById(id);
	}
	@Override
	public Customer updateCustomer(Customer customer) {
		return customerRepository.save(customer);
	}
}
